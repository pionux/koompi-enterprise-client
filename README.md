## Pionux Enterprise client
version 0.1

# How to setup

```
$ sudo mkdir /opt/pionux/
$ sudo chmod 777 /opt/pionux/
$ git clone https://hongsea@bitbucket.org/pionux/koompi-enterprise-client.git
$ cd koompi-enterprise-client/client-setup
$ sudo chmod +x setup
$ sudo ./setup
```